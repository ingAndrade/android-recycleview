package co.com.ceiba.mobile.pruebadeingreso.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapters.Adapter;
import co.com.ceiba.mobile.pruebadeingreso.model.ItemClass;
import co.com.ceiba.mobile.pruebadeingreso.services.RequestServices;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    private TextInputLayout searchUser;
    private RecyclerView recyclerView;
    List<ItemClass> itemClassList = new ArrayList<>();
    Adapter adapter;
    SharedPreferences preferences;
    private static final String PREF_UNIQUE_ID = "users";
    private static ProgressDialog progressDoalog;
    EditText Search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewSearchResults);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        preferences = getSharedPreferences(PREF_UNIQUE_ID, Context.MODE_PRIVATE);

        progressDoalog = new ProgressDialog(MainActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setTitle("Its  Loading");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();

        loadData();

        Search = findViewById(R.id.editTextSearch);
        Search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });

    }


    private void loadData(){
        Gson gson = new Gson();
        String json = preferences.getString("users", null);
        Type type = new TypeToken<ArrayList<ItemClass>>(){}.getType();
        itemClassList = gson.fromJson(json, type);

        if(itemClassList == null){
            getUsersResponde();
        }else{
            adapter = new Adapter(itemClassList);
            recyclerView.setAdapter(adapter);
            progressDoalog.dismiss();
        }
    }

    private void getUsersResponde() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Endpoints.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestServices requestServices = retrofit.create(RequestServices.class);
        Call< List<ItemClass> > call = requestServices.getUsers();

        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(MainActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setTitle("Its  Loading");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();

        call.enqueue(new Callback<List<ItemClass>>() {
            @Override
            public void onResponse(Call<List<ItemClass>> call, Response<List<ItemClass>> response) {
                progressDoalog.dismiss();
                itemClassList = new ArrayList<>(response.body());
                adapter = new Adapter(itemClassList);
                recyclerView.setAdapter(adapter);
                SaveData(itemClassList);
            }

            @Override
            public void onFailure(Call<List<ItemClass>> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });
    }

    private void SaveData(List<ItemClass> itemClassList) {
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(itemClassList);
        editor.putString("users", json);
        editor.apply();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}