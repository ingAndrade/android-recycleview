package co.com.ceiba.mobile.pruebadeingreso.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapters.AdapterPublications;
import co.com.ceiba.mobile.pruebadeingreso.model.PostClass;
import co.com.ceiba.mobile.pruebadeingreso.services.RequestServices;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostActivity extends Activity {

    public static String itemName = "nada";
    public static String itemPhone = "nada";
    public static String itemEmail = "nada";
    public static int itemUserId;
    private RecyclerView recyclerView;
    List<PostClass> postClassList = new ArrayList<>();

    private static RequestServices API_SERVICES;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        itemName = getIntent().getStringExtra("itemName");
        itemPhone = getIntent().getStringExtra("itemPhone");
        itemEmail = getIntent().getStringExtra("itemEmail");
        itemUserId = getIntent().getIntExtra("itemUserId", 0);

        TextView name = findViewById(R.id.name);
        TextView phone = findViewById(R.id.phone);
        TextView email = findViewById(R.id.email);

        name.setText(itemName);
        phone.setText(itemPhone);
        email.setText(itemEmail);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPostsResults);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        
        getUserPostResponde();
    }

    private void getUserPostResponde() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Endpoints.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestServices requestServices = retrofit.create(RequestServices.class);
        Call< List<PostClass> > call = requestServices.getUsersPost(itemUserId);

        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(PostActivity.this);
        progressDoalog.setMax(100);
        progressDoalog.setTitle("Its  Loading");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDoalog.show();

        call.enqueue(new Callback<List<PostClass>>() {
            @Override
            public void onResponse(Call<List<PostClass>> call, Response<List<PostClass>> response) {
                progressDoalog.dismiss();
                postClassList  = new ArrayList<>(response.body());
                AdapterPublications adapterPublications = new AdapterPublications(postClassList);
                recyclerView.setAdapter(adapterPublications);
            }

            @Override
            public void onFailure(Call<List<PostClass>> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
