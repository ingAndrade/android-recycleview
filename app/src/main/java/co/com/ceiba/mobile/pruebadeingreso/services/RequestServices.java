package co.com.ceiba.mobile.pruebadeingreso.services;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.model.ItemClass;
import co.com.ceiba.mobile.pruebadeingreso.model.PostClass;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestServices {

    @GET(Endpoints.GET_USERS)
    Call< List<ItemClass> > getUsers();

    @GET(Endpoints.GET_POST_USER)
    Call<List<PostClass>> getUsersPost(@Query("userId") int userId);
}
